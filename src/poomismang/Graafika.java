package poomismang;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import poomismang.HangmanGame;
import java.awt.Component;

public class Graafika extends Applet {
	/**
	 * Tavaline Hangman, graafilise liidesega
	 * 
	 * @author ekuldkep
	 */

	public Graphics g;
	HangmanGame game;

	public void paint(Graphics g) {
		this.g = g;

		this.game = new HangmanGame(this);
		game.playGame();
	}

	/**
	 * meetod, mis joonistab pea. Kasutatud on natukene praktikumis tehtud koodi
	 * 
	 */
	public void joonistaRing() {
		this.g.setColor(Color.black);
		int keskkohtX = this.getWidth() / 2;
		int keskkohtY = 110;
		int raadius = 50;

		for (double nurk = 0; nurk <= Math.PI * 2; nurk = nurk + .03) {
			int x = (int) (raadius * Math.cos(nurk));
			int y = (int) (raadius * Math.sin(nurk));
			g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
		}
	}

	/**
	 * meetod, mis joonistab keha ja jäsemed
	 * 
	 * @param x1
	 *            x alguspunkt
	 * @param y1
	 *            y alguspunkt
	 * @param x2
	 *            x lõpppunkt
	 * @param y2
	 *            y lõpppunkt
	 */
	public void joonistaJooon(int x1, int y1, int x2, int y2) {

		g.setColor(Color.black);
		int xAlguspunkt = this.getWidth() / 2 + x1;
		int yALguspunkt = 60 + y1;
		int xLõpppunkt = this.getWidth() / 2 + x2;
		int yLõpppunkt = 60 + y2;
		g.drawLine(xAlguspunkt, yALguspunkt, xLõpppunkt, yLõpppunkt);

	}

	/**
	 * joonistan poomisposti
	 * 
	 */
	public void joonistaPoomispost() {
		g.setColor(Color.black);
		g.drawLine(200, 60, 200, 10);// nöör
		g.drawLine(200, 10, 50, 10);// horisontaalne post
		g.drawLine(50, 10, 50, 400);// vertikaalne post
		g.drawLine(25, 400, 150, 400);// alus
		g.drawLine(100, 10, 50, 50);// diagonaalne post
	}

}
