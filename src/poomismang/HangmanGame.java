package poomismang;

import java.io.BufferedReader;

import java.io.FileReader;
import java.util.ArrayList;
import java.io.IOException;
import poomismang.Graafika;

public class HangmanGame {

	Graafika graafika;

	HangmanGame(Graafika graafika) {
		this.graafika = graafika;
	}

	/**
	 * meetod, mis votad asja tekitatud arrayListist suvalise sona, mida
	 * hakatakse arvama
	 * 
	 * @param Words
	 * @return
	 */
	public static String suvaline(ArrayList<String> Words) {
		int indeks = (int) (Math.random() * Words.size());
		return Words.get(indeks);
	}

	/**
	 * mangu pohiloogika
	 * 
	 */
	public void playGame() {
		Word sona = takeFromFile();
		System.out.println("Please enter your name");
		Player player1 = new Player(Player.inPut());

		while (true) {
			System.out.println(sona.formatedHiddenRepr());
			//System.out.println(sona.hiddenRepr.toString());
			

			System.out.println("Please enter a letter");
			char ch = Player.takeGuess();
			if (sona.contains(ch)) {
				System.out.println("Try again, You have already used that letter " + ch);
			}

			if (sona.replace(ch)) {
				System.out.println("Good guess!");
			} else {
				System.out.println(player1.getState());
				player1.decreaseState();
				draw(player1.getState());
				if (player1.getState() == 0) {
					System.out.println("You are out of tries. The word was" + sona.realWord);
					break;
				}
			}

			if (sona.hiddenRepr.equals(sona.realWord)) {
				System.out.println("Good job, you guessed the word");
				System.out.println("You have won this game" + player1.getWins() + 1 + "times");
				break;
			}

		}
		System.out.println("Your winningPercent is" + player1.percent(player1.getWins(), player1.getgamesPlayed() + 1));
		System.out.println("Do you want to play again. Press y");

		while (Player.inPut().equals("y")) {
			System.out.println("jouab");
			playGame();
		}

	}

	/**
	 * meetod, mis votab failist ukshaaval sonad ja lisab need arrayListi
	 * 
	 * @return tagastab sona
	 */
	public static Word takeFromFile() {
		ArrayList<String> sonad = new ArrayList<String>();

		BufferedReader luger = null;
		try {
			String sCurrentLine;
			luger = new BufferedReader(new FileReader("/home/ekuldkep/Documents/workspace/Poomismang/src/sonad.txt"));

			while ((sCurrentLine = luger.readLine()) != null) {
				sonad.add(sCurrentLine);
			}

		} catch (IOException e) {
			System.out.println("Faili ei eksisteeri");
			System.out.println(e);
		}

		String salasona = suvaline(sonad);
		Word sona = new Word(salasona);
		return sona;
	}

	/**
	 * joonistab ja tagastab vastava arvu elude korral vastava joonise
	 * 
	 * @param state
	 *            elude arv
	 */
	public void draw(int state) {
		state++;
		switch (state) {
		case 6:
			graafika.joonistaPoomispost();
			graafika.joonistaRing();
			break;
		case 5:
			graafika.joonistaJooon(0, 100, 0, 250);
			break;
		case 4:
			graafika.joonistaJooon(0, 150, 100, 200);
			break;
		case 3:
			graafika.joonistaJooon(0, 150, -100, 200);
			break;
		case 2:
			graafika.joonistaJooon(0, 250, -100, 300);
			break;
		case 1:
			graafika.joonistaJooon(0, 250, 100, 300);

		}
	}

}
