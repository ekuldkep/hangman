package poomismang;

import java.io.IOException;

import java.util.Scanner;

/**
 * klass, kuhu sisse kaivad muutujad ja meetodid, mis on seotud uhe kindla
 * mangijaga
 * 
 * @author ekuldkep
 *
 */

public class Player {

	private String name;
	private int gamesPlayed;
	private int wins;
	private double winningPercent;
	private int state = 6;

	static Scanner sc = new Scanner(System.in);

	/**
	 * construktor, mis loob mangija objekti, kui teda valja kutsuda
	 * 
	 * @param nimi
	 */
	public Player(String nimi) {
		name = nimi;
		gamesPlayed = 0;
		wins = 0;
		winningPercent = 0;
		state = 6;
	}

	/**
	 * laseb kasutajal teha pakkumise
	 * 
	 * @return teeb nii, et ainult uhe tahe saaks pakkuda
	 */
	public static char takeGuess() {

		//return sc.next(".").charAt(0);

		return inPut().charAt(0);
	}

	/**
	 * meetod, mis votab elusi maha, kui kasutaja teeb vale pakkumise
	 * 
	 * @return
	 */
	public boolean decreaseState() {
		state--;

		return false;

	}

	/**
	 * state getter
	 * 
	 * @return tagastab state
	 */
	public int getState() {
		return state;
	}

	/**
	 * laseb kasutajal nime sisestada
	 * 
	 * @return tagastab kasutajasisestuse
	 */
	public static String inPut() {
		String userInput;
		userInput = sc.nextLine();

		return userInput;

	}

	/**
	 * voitude getter
	 * 
	 * @return
	 */
	public int getWins() {
		return wins++;
	}

	public int getgamesPlayed() {
		return gamesPlayed++;
	}

	/**
	 * meetod, mis arvutab protsendi
	 * 
	 * @param wins
	 * @param gamesPlayed
	 * @return
	 */
	public double percent(int wins, int gamesPlayed) {
		this.wins = wins;
		this.gamesPlayed = gamesPlayed;
		double winningPercent = wins * 100 / gamesPlayed;
		return winningPercent;

	}

}
