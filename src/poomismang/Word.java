package poomismang;

import java.util.ArrayList;

public class Word {
	ArrayList<Character> hiddenRepr = new ArrayList<Character>();
	ArrayList<Character> realWord = new ArrayList<Character>();
	ArrayList<Character> suggestedLetters = new ArrayList<Character>();
	String formatedString;

	/**
	 * meetod, mis votab selle suvaliselt voetud sona, teeb sonapikkusele
	 * vastava arvu kriipse ja lisab need molemad omaette arrayListi
	 * 
	 * @param word
	 *            sona, millega ta hakkab eelenvalt kirjutatud asju tegema
	 */
	public Word(String word) {

		char[] wordC = word.toCharArray();
		for (int i = 0; i < word.length(); i++) {
			hiddenRepr.add('_');
			realWord.add(wordC[i]);

		}

	}

	public  String formatedHiddenRepr() {
		
		return hiddenRepr.toString()
		.replace(",", "") // remove the commas
		.replace("[", "") // remove the right bracket
		.replace("]", "") // remove the left bracket
		.trim();
		
	}

	/**
	 * meetod, mis kontrollib, kas tegelikus sonas esineb kasutajasisestus kui
	 * esineb, siis asendab kriipsud vastavas kohas tahtedega
	 * 
	 * @param ch
	 *            kasutajasisestus
	 * @return tagastab toese, kui sonas on kasutaja pakutud taht
	 */
	public boolean replace(Character ch) {
		suggestedLetters.add(ch);
		if (realWord.contains(ch)) {

			for (int i = 0; i < realWord.size(); i++) {

				if (realWord.get(i).equals(ch)) {
					hiddenRepr.set(i, ch);

				}
			}

			return true;

		}
		return false;

	}

	/**
	 * meetod, mis kontrollib, et kasutaja ei saaks mitu korda sama tahte
	 * sisestada
	 * 
	 * @param ch
	 *            kasutajasisestus
	 * @return tagastab toese, kui kasutaja on seda tahte juba pakkunud
	 */
	public boolean contains(Character ch) {
		return suggestedLetters.contains(ch);
	}

}
